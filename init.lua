local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local opt = vim.opt

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd 'packadd packer.nvim'
end

require('packer').startup(function()
  use 'wbthomason/packer.nvim'
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/nvim-compe'
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use 'lukas-reineke/indent-blankline.nvim'
  use {'hoob3rt/lualine.nvim', requires = {'kyazdani42/nvim-web-devicons', opt = true}}
  use 'navarasu/onedark.nvim'
end)

require('lspconfig')
require('compe')
require('nvim-treesitter')
require('indent_blankline')
require('lualine')
require('onedark')

opt.number = true
opt.cursorline = true
opt.mouse = 'a'
opt.termguicolors = true
opt.background = 'dark'
opt.clipboard = 'unnamedplus'
opt.swapfile = false
opt.hidden = true
opt.linebreak = true
opt.expandtab = true
opt.tabstop = 2
opt.softtabstop = 2
opt.shiftwidth = 2
opt.completeopt = 'menuone,noselect'
